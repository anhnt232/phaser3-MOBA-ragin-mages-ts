import BaseScene from './BaseScene';
import Button from '../objects/ui/Button';
import DOMModal from '../objects/ui/DOMModal';
import Controller from '../util/Controller';
import Character from '../objects/Character';

export default class CharacterSelectionScene extends BaseScene {
  gameType: string;
  controller: Controller;
  characterBackdrop: Phaser.GameObjects.Graphics;
  playerHandle: Phaser.GameObjects.Text;
  chosenCharacter: Character;
  selectedCharacterKey: string;
  constructor() {
    super({key: 'CharacterSelectionScene'});
    
  }

  init(data: { type: string; }){
    this.gameType = data.type;
  }

  preload() {
    this.controller = new Controller(this);
    // this.scaleToFit();
  }

  create() {
    this.chosenCharacter = null;
    // let background = this.add.image(800, 330, 'title_background');
    // this.cameras.main.startFollow(background);
    let x = this.scale.width/10;
    let y = this.scale.height/10;
    let logoStyle = {
      fontSize: '40px', 
      fontFamily: "'Jim Nightshade', cursive", 
      color: '#000000'};
    let logo = this.add.text(x, y-30, 'Ragin\' Mages', logoStyle);
    logo.setStroke('#ae7f00', 10);
    let playerMode= this.gameType == 'single_player' ? 'single player mode' : 'multiplayer on-line battle mode';

    this.add.text(x, y + 20, 'Select your character for ' + playerMode, {
      fontSize: '18px',
      fontFamily: "'Fjalla One', sans-serif",
      backgroundColor: '#ae7f00'
    });

    let rw = 300;
    this.characterBackdrop = this.add.graphics();
    this.characterBackdrop.x = this.scale.width/2;
    this.characterBackdrop.y = this.scale.height/2-rw/2;
    this.characterBackdrop.fillStyle(0xffffff, 0.9);
    this.characterBackdrop.fillRect(0, 0, rw, rw);

    let selectButton = new Button(this, this.scale.width/2, 10, 'Select', {width: 150});
    selectButton.scene = this;
    selectButton.buttonDown(() => {
      let handle = this.playerHandle ? this.playerHandle.text : null;
      this.changeToScene(this.gameType == 'multi_player' ? 'GameScene' : 'DungeonScene', {
        character: this.selectedCharacterKey, 
        playerHandle: handle});
    });

    let btnX = x;
    let btnY = y + 60;
    let btnSpacing = 35;

    let characterList = this.cache.json.get('characters');
    for (const key in characterList) {
      this.addCharacterButton(characterList[key], this, btnX, btnY);
      btnY += btnSpacing;
    }

    if(this.gameType == 'multi_player') {
      const defaultName = localStorage.getItem('name') || 'Player' + Math.floor(Math.random() * 9999);
      this.playerHandle = this.add.text(this.characterBackdrop.x+150, this.characterBackdrop.y + 300 + 10, defaultName, {
        fontSize: '28px',
        fontFamily: "'Fjalla One', sans-serif",
        backgroundColor: '#ae7f00',
      });
      this.playerHandle.setOrigin(0.5, 0);

      let nameInputButton = new Button(this, this.characterBackdrop.x+150, this.characterBackdrop.y + 300 + 55, 'Change Name', {width: 165});
      nameInputButton.setOrigin(0.5, 0);
      nameInputButton.buttonDown(() => {
        // this.input.keyboard.stopListeners();
        new DOMModal(this, 'nameSelection', {
          width: 'auto',
          cancelButtonSelector: '#cancel',
          acceptButtonSelector: '#accept',
          onCancel: (modal) => {
            modal.close();
            // this.input.keyboard.startListeners();
          },
          onAccept: (modal) => {
            const name = modal.modal.querySelector('input').value;
            localStorage.setItem('name', name);
            this.playerHandle.setText(name);
            modal.close();
            // this.input.keyboard.startListeners();
          },
          data: {
            handle: this.playerHandle.text
          }
        });
      });
    }
  }

  addCharacterButton(btnData: { 
    name: string; 
    key: string; 
  }, 
    scene: Phaser.Scene, 
    x: number, 
    y: number){
    let chkButton = new Button(this, x, y, btnData.name, {width: 300});
    chkButton.scene = scene;
    chkButton.buttonDown(() => {
      this.selectedCharacterKey = btnData.key;
      this.showCharacter(this.selectedCharacterKey);
    });

    chkButton.on('pointerover', () => {
      this.selectedCharacterKey = btnData.key;
      this.showCharacter(this.selectedCharacterKey);
    });

    let backToMenuButton = new Button(this, x, this.scale.height - 50, 'BACK', {
      width: 100,
      fontColorNormal: '#ffffff'
    });
    backToMenuButton.buttonDown(() => {
      this.changeToScene('TitleScene');
    });
  }

  showCharacter(key: string) {
    if(this.chosenCharacter) this.chosenCharacter.destroy();
    this.chosenCharacter = new Character(this, this.characterBackdrop.x + 150, this.characterBackdrop.y + 300 * 0.8, key, null, {
      scale: 1,
      orientation: 'S',
      hideHealth: true
    });
  }

  update() {
    if(this.chosenCharacter) {
      const orientation = this.controller.getWASDCoordinate();
      console.log("orientation", orientation);
      console.log(this.controller);
      this.chosenCharacter.setAnimation(orientation != '' ? 'walk' : 'stance', orientation);
    }
  }

  destroy(){
    if(this.chosenCharacter){
      this.chosenCharacter.destroy();
    }
  }

}