import 'phaser';
import Server from '../services/Server';

export default class BaseScene extends Phaser.Scene {
  server: Server;

  constructor(key: string | Phaser.Types.Scenes.SettingsConfig) {
    super(key);
    this.server = new Server();
  }

  changeToScene(key: string | Phaser.Scene, data = null) {
    this.scene.stop("GamepadScene");
    this.input.keyboard.removeAllListeners();
    this.input.removeAllListeners();
    this.scene.stop();
    this.scene.start(key, data);
  }

  scaleToFit(width = 700, height = 650) {
    let scaleX = window.innerWidth < width ? window.innerWidth / width : 1;
    let scaleY = window.innerHeight < height ? window.innerHeight / height : 1;
    if(scaleX < scaleY) {
      this.cameras.main.setZoom(scaleX);
    }
    else if(scaleY < scaleX) {
      this.cameras.main.setZoom(scaleY);
    }
  }
}