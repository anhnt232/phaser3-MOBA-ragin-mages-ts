import BaseScene from './BaseScene';
import Button from '../objects/ui/Button';
import DOMModal from '../objects/ui/DOMModal';

export default class TitleScene extends BaseScene {
  multiPlayerButton: Button;

  constructor() {
    super({key: 'TitleScene'});
  }

  preload() {
    // let serverConfig = this.cache.json.get('config');
    // this.server.connect(serverConfig.protocol, serverConfig.host, serverConfig.port);
    // this.server.requestEvents();
    // this.server.on('serverConnected', this.serverConnected, this);
    // this.server.on('serverDisconnected', this.serverDisconnected, this);

    // this.scaleToFit();
  }

  create() {
    // let background = this.add.image(800, 330, 'title_background');
    // this.cameras.main.startFollow(background);
    let x = this.scale.width/10;
    let y = this.scale.height/10;
    let logoStyle = {
      fontSize: '40px', 
      fontFamily: "'Jim Nightshade', cursive", 
      color: '#000000'};
    let logo = this.add.text(x, y-30, 'Ragin\' Mages', logoStyle);
    logo.setStroke('#ae7f00', 10);

    //multi player button
    this.multiPlayerButton = new Button(this, x, y + 100, 'PLAY MULTI PLAYER', {
      // disabled: !this.server.isConnected()
      disabled: true
    });
    this.multiPlayerButton.buttonDown(() => {
      this.changeToScene('CharacterSelectionScene', { type: 'multi_player' });
    });

    //single player button
    let singlePlayerButton = new Button(this, x, y + 170, 'PLAY SINGLE PLAYER');
    singlePlayerButton.buttonDown(() => {
      this.changeToScene('CharacterSelectionScene', {type: 'single_player'});
    });

    //controls, credits, offline mode buttons
    // if(ServiceWorker.isSupported()) {
    //   const assets = this.cache.json.get('assets');
    //   let serviceWorker = new ServiceWorker();
    //   let settingsButton = new Button(this, 450, 350, 'SETTINGS');

    //   settingsButton.buttonDown(() => {
    //     new DOMModal(this, 'settings', {
    //       cancelButtonSelector: '.exit',
    //       acceptButtonSelector: '#settingsCheck',
    //       onCancel: (modal) => {
    //         modal.close();
    //       },
    //       onAccept: (modal) => {            
    //         if (modal.modal.querySelector('#settingsCheck').checked) {
    //           serviceWorker.register().then(() => {
    //             serviceWorker.cacheAssets(assets);
    //           });
    //         }
    //         else {
    //           serviceWorker.unregister();
    //         }
    //       },
    //       data: {offlineMode: serviceWorker.isRegistered()}
    //     });
    //   });
    // }
    let creditsButton = new Button(this, x, y + 230, 'CREDITS');
    creditsButton.buttonDown(() => {
      new DOMModal(this, 'credits', {
        cancelButtonSelector: '.exit',
        closeOnBackdropClick: true,
        onCancel: (modal) => {
          modal.close();
        }
      });
    });

    let controlsButton = new Button(this, x, y + 300, 'HOW TO PLAY');
    controlsButton.buttonDown(() => {
      new DOMModal(this, 'controls', {
        cancelButtonSelector: '.exit',
        closeOnBackdropClick: true,
        onCancel: (modal) => {
          modal.close();
        }
      });
    });

  }

  update() {
  }

  serverConnected() {
    this.multiPlayerButton.setDisabled(false);
  }

  serverDisconnected() {
    this.multiPlayerButton.setDisabled(true);
  }
}