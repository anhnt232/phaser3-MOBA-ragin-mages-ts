import 'phaser';
export default class Checkbox extends Phaser.GameObjects.Group {
  tickbox: Phaser.GameObjects.Image;
  label: Phaser.GameObjects.Text;
  checked: boolean;
  constructor(scene: Phaser.Scene, x: number, y: number, text: string | string[], defaultChecked: boolean) {
    const scale = 0.3;
    const fontSize = 30 //* scale;
 
    let tickbox = scene.add.image(x, y, 'checkmark', defaultChecked ? 'checked' : 'unchecked');
    tickbox.setScale(scale);
    let label = scene.add.text(x + tickbox.displayWidth - 4, y - fontSize / 1.4, text, {
      fontSize: fontSize.toString(),
      fontFamily: "'Fjalla One', sans-serif",
      backgroundColor: '#d3d3d3'
    });
    super(scene, [tickbox, label]);

    this.tickbox = tickbox;
    this.label = label;

    label.setStroke('#00000', 6);
    this.tickbox.setInteractive();
    this.checked = defaultChecked == true;

    scene.add.existing(this);

    this.tickbox.on('pointerdown', () => {
      this.toggle();
    });


  }

  isChecked() {
    return this.checked;
  }

  toggle() {
    this.setChecked(!this.checked);
  }

  setChecked(checked: boolean) {
    this.checked = checked == true;
    this.tickbox.setFrame(this.checked ? 'checked' : 'unchecked');
  }

  onPointerDown(handler: (arg0: this) => void) {
    this.tickbox.on('pointerdown', () => {
      handler(this);
    });
  }
}