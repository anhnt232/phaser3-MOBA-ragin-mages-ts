import 'phaser';
export default class Controller {
  cursors: any;
  keyW: Phaser.Input.Keyboard.Key;
  keyA: Phaser.Input.Keyboard.Key;
  keyS: Phaser.Input.Keyboard.Key;
  keyD: Phaser.Input.Keyboard.Key;
  virtualKeys: { 
    up: { isDown: boolean; }; 
    down: { isDown: boolean; }; 
    left: { isDown: boolean; }; 
    right: { isDown: boolean; }; 
  };
  constructor(scene: Phaser.Scene) {
    this.cursors = scene.input.keyboard.createCursorKeys();
    this.addWASDKeys(scene);
    this.addVirtualKeys();
  }

  addWASDKeys(scene: Phaser.Scene) {
    this.keyW = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    this.keyA = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    this.keyS = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
    this.keyD = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
  }

  addVirtualKeys() {
    this.virtualKeys = {
      up: { isDown: false },
      down: { isDown: false },
      left: { isDown: false },
      right: { isDown: false }
    };
  }

  setVirtualKeys(keys: { up: { isDown: boolean; } | { isDown: boolean; }; down: { isDown: boolean; } | { isDown: boolean; }; left: { isDown: boolean; } | { isDown: boolean; }; right: { isDown: boolean; } | { isDown: boolean; }; }) {
    this.virtualKeys = keys;
    console.log("Set Virtual Key", keys);
  }

  getWASDCoordinate() {
    let direction = this.cursors.down.isDown || this.keyS.isDown || this.virtualKeys.down.isDown
      ? 'S' : (this.cursors.up.isDown || this.keyW.isDown || this.virtualKeys.up.isDown ? 'N' : '');
    direction += this.cursors.left.isDown || this.keyA.isDown || this.virtualKeys.left.isDown
      ? 'W' : (this.cursors.right.isDown || this.keyD.isDown || this.virtualKeys.right.isDown ? 'E' : '');

    return direction;
  }

  getWASDVector() {
    let x = 0;
    let y = 0;

    if(this.cursors.up.isDown || this.keyW.isDown || this.virtualKeys.up.isDown) {
      y = -1;

    }
    if(this.cursors.down.isDown || this.keyS.isDown || this.virtualKeys.down.isDown){
      y += 1;
    }

    if(this.cursors.left.isDown || this.keyA.isDown || this.virtualKeys.left.isDown) {
      x = -1;
    }
    if(this.cursors.right.isDown || this.keyD.isDown || this.virtualKeys.right.isDown) {
      x += 1;
    }

    return new Phaser.Math.Vector2(x, y).normalize();
  }


}