import BootScene from './scenes/BootScene';
import LoaderScene from './scenes/LoaderScene';
import TitleScene from './scenes/TitleScene';
import CharacterSelectionScene from './scenes/CharacterSelectionScene';
import GameScene from './scenes/GameScene';
import GamepadScene from './scenes/GamepadScene';
import DungeonScene from './scenes/DungeonScene';

let game : Phaser.Game;

window.onload = function() {

  let gameConfig = {
    type: Phaser.WEBGL,
    fps: {
      target: 20,
    },
    backgroundColor: 0xB6D3FF,
    scale: {
      mode: Phaser.Scale.FIT,
      autoCenter: Phaser.Scale.CENTER_BOTH,
      parent: "thegame",
      width: 844,
      height: 390,
      // width: window.innerWidth,
      // height: window.innerHeight,
    },
    physics: {
      default: 'arcade',
      arcade: {
        gravity: { y: 0 },
        debug: false
      }
    },
    
    scene: [BootScene, LoaderScene, TitleScene, CharacterSelectionScene, GameScene, DungeonScene, GamepadScene]

  }
  game = new Phaser.Game(gameConfig);

}
